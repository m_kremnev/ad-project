import Vue from 'vue';
import App from './App.vue';
import vuetify from './plugins/vuetify';
import router from './router';
import * as fb from 'firebase';
import BuyModalComponent from '@/components/Shared/BuyModal';
import 'vue-material-design-icons/styles.css';
import store from './store';

Vue.use(vuetify);
Vue.component('app-buy-modal', BuyModalComponent);
Vue.config.productionTip = false;
new Vue({
  vuetify,
  router,
  store,
  render: (h) => h(App),
  created() {
    fb.initializeApp({
      apiKey: 'AIzaSyBuRWu4Xm54WL22dCcFKWq0jpgMZ-KuMRk',
      authDomain: 'first-project-f9f13.firebaseapp.com',
      databaseURL: 'https://first-project-f9f13.firebaseio.com',
      projectId: 'first-project-f9f13',
      storageBucket: 'first-project-f9f13.appspot.com',
      messagingSenderId: '1070162933188',
      appId: '1:1070162933188:web:8d69685c1311c199cca091',
    });
    fb.auth().onAuthStateChanged((user) => {
      if (user) {
        this.$store.dispatch('autoLoginUser', user);
      }
    });
    this.$store.dispatch('fetchAds');
  },
}).$mount('#app');
